package lesson5;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ActionsLesson5 {
    WebDriver wb;
    WebDriverWait wbWait;

    @BeforeAll
    static void setupDriver () {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeEach
    void initBr () {
        wb = new ChromeDriver();
        wbWait = new WebDriverWait(wb, 5);
        login(wb);
    }

    void dragAndDropTest() {
        wb.get("https://crm.geekbrains.space/dashboard");
            }

    @AfterEach
    void TearDoun() {
        wb.quit();
    }

    static void login(WebDriver wb) {
        wb.get("https://crm.geekbrains.space/user/login");
        wb.findElement(By.id("prependedInput")).sendKeys("Applanatest1");
        wb.findElement(By.id("prependedInput2")).sendKeys("Student2020!");
        wb.findElement(By.id("_submit")).click();

    }

}

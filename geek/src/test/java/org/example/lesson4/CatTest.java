package org.example.lesson4;

public class CatTest {
    private String name;
    private int ege;

    public CatTest(String name, int ege) {
        this.name = name;
        this.ege = ege;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEge() {
        return ege;
    }

    public void setEge(int ege) {
        this.ege = ege;
    }

    @Override
    public String toString() {
        return "CatTest{" +
                "name='" + name + '\'' +
                ", ege=" + ege +
                '}';
    }
}

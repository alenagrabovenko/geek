package org.example.lesson4.HomeWork4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.example.lesson4.Triangle.calcSquare;

public class lesson4TriangleTest {
    @Test
    void positiveTest() {
        double result = calcSquare(8,8,8);
        Assertions.assertEquals(27.712812921102035, result);
    }

    @Test
    void negativeYest() {
        double result = calcSquare(-9,9,9);
        Assertions.assertEquals(0, result);
    }
}

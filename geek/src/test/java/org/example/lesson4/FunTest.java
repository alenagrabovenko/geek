package org.example.lesson4;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class FunTest {
    @BeforeAll
    static void beforeOllMethod() {
        System.out.println("Метод выполняется 1 раз перед всеми тестами сайта");
    }

    @BeforeEach
    void beforeEachMethod() {
        System.out.println("Метод выполнится перед каждым тестом");
    }

    @ParameterizedTest
    @ValueSource(ints = {1,7,11})

    @DisplayName("Позитиный тест простого числа")
    void givenPrimeNumberWhenCheckIsPrimeThenTrue(int number) {
        boolean result = Functions.isNumberPrime(number);
        Assertions.assertTrue(result);
    }

    @ParameterizedTest
    @CsvSource({"1, true", "6, false", "12, false"})
    @DisplayName("Негативный тест простого числа")
    void givenNotPrimeNumberWhenCheckIsPrimeThenFolse(int number, boolean result) {
        Assertions.assertEquals(result, Functions.isNumberPrime(number));
    }

    @ParameterizedTest
    @MethodSource("catDataProvider")
    void testCat(CatTest cat, boolean result) {
        System.out.println(cat);
        System.out.println(result);
    }

    private static Stream<Arguments> catDataProvider() {
        return Stream.of(Arguments.arguments(new CatTest("Dixi", 10), true),
                Arguments.arguments(new CatTest("Pixi", 15), false));

    }

    @AfterEach
    void afterEach() {
        System.out.println("Метод будет выполняться после каждого теста");
    }

    @AfterAll
    static void aftrrAll() {
        System.out.println("Медод выполнится 1 раз после всех тестов");
    }

}

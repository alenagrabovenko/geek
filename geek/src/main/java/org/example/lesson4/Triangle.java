package org.example.lesson4;

public class Triangle {
    public static double calcSquare(double a, double b, double c) {
        if (a <=0 || b <= 0 || c <= 0) return 0;
        double halfP = (a + b + c) / 2;
        return Math.sqrt(halfP * (halfP - a) * (halfP - b) * (halfP - c));
    }
}

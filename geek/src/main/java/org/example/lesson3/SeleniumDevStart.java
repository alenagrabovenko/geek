package org.example.lesson3;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumDevStart {
    public static void main(String[] args) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://nevelin-samara.nevel.online/");
        driver.quit();

        WebDriverManager.firefoxdriver().setup();
        WebDriver ffdriver = new FirefoxDriver();
        ffdriver.get("https://nevelin-samara.nevel.online/");

        ffdriver.quit();

    }
}
